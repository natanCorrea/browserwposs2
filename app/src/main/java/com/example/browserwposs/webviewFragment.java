package com.example.browserwposs;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;


public class webviewFragment extends Fragment {

    WebView webView;

    public webviewFragment() {
        // Required empty public constructor

    }



    public static webviewFragment newInstance(String param1, String param2) {
        webviewFragment fragment = new webviewFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista=inflater.inflate(R.layout.fragment_webview, container, false);

        webView=(WebView)vista.findViewById(R.id.webview);
        String URL="https://benjagarrido.com/compartiendo-un-proyecto-de-android-studio-en-github/";
        webView.loadUrl(URL);

        return vista;
    }
}