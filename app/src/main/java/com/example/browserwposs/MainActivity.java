package com.example.browserwposs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import static com.example.browserwposs.R.id.contenedor;


public class MainActivity extends AppCompatActivity {
    WebView webView;

    FragmentTransaction transaction;
    Fragment webviewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webviewFragment =new webviewFragment();
        getSupportActionBar().hide();
        getSupportFragmentManager().beginTransaction().add(contenedor,webviewFragment).addToBackStack(null).commit();


    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.edit_url);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}